--
-- Host: localhost    Database: padshop
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cart` (
  `cart_id` int NOT NULL AUTO_INCREMENT,
  `quantity` int NOT NULL,
  `quantity_id` int NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `FK_cart_product_quantity` (`quantity_id`),
  KEY `FK_cart_usertbl` (`user_id`),
  CONSTRAINT `FK_cart_product_quantity` FOREIGN KEY (`quantity_id`) REFERENCES `product_quantity` (`quantity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cart_usertbl` FOREIGN KEY (`user_id`) REFERENCES `usertbl` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
INSERT INTO `cart` VALUES (2,2,83,24),(6,3,128,21),(7,2,99,21),(8,20,119,21),(9,20,131,24),(10,4,130,24),(11,3,132,24),(12,2,86,7);
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_address`
--

DROP TABLE IF EXISTS `order_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_address` (
  `address_id` int NOT NULL AUTO_INCREMENT,
  `province` varchar(50) NOT NULL,
  `district` varchar(50) NOT NULL,
  `village` varchar(50) NOT NULL,
  `detail` varchar(50) NOT NULL,
  `order_id` int NOT NULL,
  PRIMARY KEY (`address_id`),
  KEY `FK_address_order_order` (`order_id`),
  CONSTRAINT `FK_address_order_order` FOREIGN KEY (`order_id`) REFERENCES `ordertbl` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_address`
--

LOCK TABLES `order_address` WRITE;
/*!40000 ALTER TABLE `order_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_item`
--

DROP TABLE IF EXISTS `order_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_item` (
  `item_id` int NOT NULL AUTO_INCREMENT,
  `color` varchar(50) DEFAULT NULL,
  `size` varchar(50) DEFAULT NULL,
  `price` int DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `quantity_id` int DEFAULT NULL,
  `order_id` int DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `FK_tblorder_item_tblorder` (`order_id`),
  KEY `FK_tblorder_item_tblproduct_idx` (`quantity_id`),
  CONSTRAINT `FK_tblorder_item_tblorder` FOREIGN KEY (`order_id`) REFERENCES `ordertbl` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_item`
--

LOCK TABLES `order_item` WRITE;
/*!40000 ALTER TABLE `order_item` DISABLE KEYS */;
INSERT INTO `order_item` VALUES (2,'xanh','xl',295000,2,16,2),(3,'trắng','m',295000,1,16,2),(4,'xanh','xl',295000,3,16,3),(9,'be','xl',500000,1,111,6),(10,'be','xl',500000,2,111,7),(11,'đên','l',299000,12,128,8),(12,'trắng','xl',290000,2,99,8),(13,'đên','l',299000,3,128,9),(14,'trắng','xl',290000,2,99,9),(15,'đên','l',299000,3,128,10),(16,'trắng','xl',290000,2,99,10),(17,'đên','l',299000,6,128,11),(18,'trắng','xl',290000,2,99,11),(19,'đên','l',299000,4,128,12),(20,'trắng','xl',290000,2,99,12),(21,'đen','l',595000,17,119,13),(22,'đen','xl',0,5,114,14),(23,'','xl',0,5,108,15),(24,'đên','l',299000,1,128,16),(42,'đên','l',299000,1,128,34),(46,'đên','l',299000,1,128,38),(47,'trắng','l',299000,1,125,39),(48,'trắng','l',299000,1,125,40),(52,'trắng','m',290000,2,85,44),(53,'xanh','l',295000,4,83,45),(54,'','l',399999,20,131,45),(55,'','m',399999,4,130,45),(57,'xanh','l',295000,2,83,47),(58,'','l',399999,20,131,47),(59,'','m',399999,4,130,47),(60,'','xl',399999,3,132,47),(61,'trắng','l',290000,2,86,48),(63,'','l',399999,1,131,50),(64,'','l',399999,1,131,51),(67,'đên','l',299000,1,128,54),(68,'','l',399999,1,131,55),(70,'','XL',555000,1,137,57),(71,'','l',399999,1,131,58),(72,'','l',399999,1,131,59),(73,'','m',399999,1,130,60),(74,'','l',399999,1,131,61),(75,'','XL',555000,1,137,62),(76,'','m',399999,1,130,63),(77,'','l',399999,1,131,64),(78,'','m',399999,1,130,65),(79,'','l',399999,1,131,66);
/*!40000 ALTER TABLE `order_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordertbl`
--

DROP TABLE IF EXISTS `ordertbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ordertbl` (
  `order_id` int NOT NULL AUTO_INCREMENT,
  `buyer_name` varchar(50) DEFAULT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `buying_date` date DEFAULT NULL,
  `discount` int DEFAULT NULL,
  `pay_total` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  `ward` varchar(50) DEFAULT NULL,
  `detail` varchar(50) DEFAULT NULL,
  `payment` int DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `FK_order_user` (`user_id`),
  CONSTRAINT `FK_order_user` FOREIGN KEY (`user_id`) REFERENCES `usertbl` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordertbl`
--

LOCK TABLES `ordertbl` WRITE;
/*!40000 ALTER TABLE `ordertbl` DISABLE KEYS */;
INSERT INTO `ordertbl` VALUES (2,'Anh Duc','0364743279','2022-07-05',NULL,885000,1,21,'Tỉnh Bắc Giang','Huyện Lục Nam','Xã Nghĩa Phương','thôn Phượng Hoàng',0),(3,'','','2022-07-06',NULL,885000,2,21,'','','','',0),(6,'Phuong Thuy','0333333333','2022-07-06',NULL,500000,2,21,'Tỉnh Hà Giang','Huyện Yên Minh','Xã Du Già','PTIT.',0),(7,'Anh Duc','0364743279','2022-07-06',NULL,1000000,2,21,'Tỉnh Bắc Giang','Huyện Lục Nam','Xã Nghĩa Phương','thôn Phượng Hoàng',0),(8,'Anh Tu','3433333333','2022-07-06',NULL,4168000,2,21,'Thành phố Hà Nội','Huyện Thanh Trì','Xã Duyên Hà','PTIT.',0),(9,'Anh Duc','0364743279','2022-07-06',NULL,1477000,2,21,'Tỉnh Bắc Giang','Huyện Lục Nam','Xã Nghĩa Phương','thôn Phượng Hoàng',0),(10,'Anh Duc','0364743279','2022-07-06',NULL,1477000,2,21,'Tỉnh Bắc Giang','Huyện Lục Nam','Xã Nghĩa Phương','thôn Phượng Hoàng',0),(11,'Phuong Thuy','0333333333','2022-07-06',NULL,2374000,2,21,'Tỉnh Hà Giang','Huyện Yên Minh','Xã Du Già','PTIT.',0),(12,'Anh Duc','0364743279','2022-07-06',NULL,1776000,2,21,'Tỉnh Bắc Giang','Huyện Lục Nam','Xã Nghĩa Phương','thôn Phượng Hoàng',0),(13,'Anh Duc','0364743279','2022-07-15',NULL,10115000,1,21,'Tỉnh Bắc Giang','Huyện Lục Nam','Xã Nghĩa Phương','thôn Phượng Hoàng',0),(14,'Admin','0364743279','2023-05-04',NULL,200000,2,24,'Tỉnh Bắc Giang','Huyện Lục Ngạn','Xã Hồng Giang','PTIT-A2',0),(15,'Đức Admin','0364743279','2023-04-18',NULL,0,1,24,'Thành phố Hà Nội','Quận Hà Đông','Phường Mộ Lao','PTIT-A3',0),(16,'Admin','0364743279','2023-03-05',NULL,299000,2,24,'Tỉnh Bắc Giang','Huyện Lục Ngạn','Xã Hồng Giang','PTIT-A2',0),(34,'Đức Admin','0364743279','2023-05-05',NULL,299000,1,24,'Thành phố Hà Nội','Quận Hà Đông','Phường Mộ Lao','PTIT-A3',0),(38,'PHuong thuy','0364743279','2023-05-05',NULL,299000,1,24,'Tỉnh Bắc Giang','Huyện Lục Ngạn','Xã Hồng Giang','PTIT-A2',1),(39,'Admin','0364743279','2023-05-05',NULL,299000,1,24,'Tỉnh Bắc Giang','Huyện Lục Ngạn','Xã Hồng Giang','PTIT-A2',1),(40,'Admin','0364743279','2023-05-05',NULL,299000,1,24,'Tỉnh Bắc Giang','Huyện Lục Ngạn','Xã Hồng Giang','PTIT-A2',1),(44,'Test Nhom','0123456789','2023-05-05',NULL,580000,1,3,'Tỉnh Yên Bái','Huyện Trấn Yên','Xã Hưng Khánh','Vân Tảo',1),(45,'Admin','0364743279','2023-05-05',NULL,10779976,2,24,'Tỉnh Bắc Giang','Huyện Lục Ngạn','Xã Hồng Giang','PTIT-A2',1),(47,'Admin','0364743279','2023-05-05',NULL,11389973,1,24,'Tỉnh Bắc Giang','Huyện Lục Ngạn','Xã Hồng Giang','PTIT-A2',1),(48,'Dat Hoang','0367436886','2023-05-05',NULL,580000,1,7,'Tỉnh Cao Bằng','Huyện Quảng Hòa','Xã Cách Linh','PTIT-A2',1),(50,'Admin','0364743279','2023-05-12',NULL,399999,2,24,'Tỉnh Bắc Giang','Huyện Lục Ngạn','Xã Hồng Giang','PTIT-A2',0),(51,'Admin','0364743279','2023-05-12',NULL,399999,2,24,'Tỉnh Bắc Giang','Huyện Lục Ngạn','Xã Hồng Giang','PTIT-A2',0),(54,'Đức Admin','0364743279','2023-05-12',NULL,299000,2,24,'Thành phố Hà Nội','Quận Hà Đông','Phường Mộ Lao','PTIT-A3',0),(55,'Đức Admin','0364743279','2023-05-12',NULL,399999,2,24,'Thành phố Hà Nội','Quận Hà Đông','Phường Mộ Lao','PTIT-A3',0),(57,'Đức Admin','0364743279','2023-05-12',NULL,555000,2,24,'Thành phố Hà Nội','Quận Hà Đông','Phường Mộ Lao','PTIT-A3',0),(58,'Đức Admin','0364743279','2023-05-12',NULL,399999,2,24,'Thành phố Hà Nội','Quận Hà Đông','Phường Mộ Lao','PTIT-A3',0),(59,'Đức Admin','0364743279','2023-05-12',NULL,399999,2,24,'Thành phố Hà Nội','Quận Hà Đông','Phường Mộ Lao','PTIT-A3',0),(60,'Đức Admin','0364743279','2023-05-12',NULL,399999,2,24,'Thành phố Hà Nội','Quận Hà Đông','Phường Mộ Lao','PTIT-A3',0),(61,'Đức Admin','0364743279','2023-05-12',NULL,399999,-1,24,'Thành phố Hà Nội','Quận Hà Đông','Phường Mộ Lao','PTIT-A3',0),(62,'Đức Admin','0364743279','2023-05-12',NULL,555000,-1,24,'Thành phố Hà Nội','Quận Hà Đông','Phường Mộ Lao','PTIT-A3',0),(63,'Đức Admin','0364743279','2023-05-12',NULL,399999,-1,24,'Thành phố Hà Nội','Quận Hà Đông','Phường Mộ Lao','PTIT-A3',0),(64,'Đức Admin','0364743279','2023-05-12',NULL,399999,-1,24,'Thành phố Hà Nội','Quận Hà Đông','Phường Mộ Lao','PTIT-A3',0),(65,'Đức Admin','0364743279','2023-05-12',NULL,399999,-1,24,'Thành phố Hà Nội','Quận Hà Đông','Phường Mộ Lao','PTIT-A3',0),(66,'Admin','0364743279','2023-05-23',NULL,399999,1,24,'Tỉnh Bắc Giang','Huyện Lục Ngạn','Xã Hồng Giang','PTIT-A2',0);
/*!40000 ALTER TABLE `ordertbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `product_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `price` int NOT NULL,
  `code` varchar(50) NOT NULL,
  `promotion_id` int DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  KEY `FK_product_promotion` (`promotion_id`),
  CONSTRAINT `FK_product_promotion` FOREIGN KEY (`promotion_id`) REFERENCES `promotion` (`promotion_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (16,'Áo phông ',295000,'APH16',3),(17,'Áo phông',290000,'APH17',3),(18,'Áo phông',290000,'APH18',NULL),(19,'áo phông',290000,'APH19',NULL),(20,'áo phông đen',255000,'APH20',4),(21,'Áo polo',455000,'APL21',4),(22,'Áo polo',500000,'APL22',4),(23,'Áo polo trẻ trung',595000,'APL23',4),(24,'Áo polo',299000,'APL24',NULL),(25,'Áo polo trẻ trung',399999,'APL25',NULL),(27,'Quần Jean KBS',555000,'QJE27',NULL);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_image`
--

DROP TABLE IF EXISTS `product_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_image` (
  `image_id` int NOT NULL AUTO_INCREMENT,
  `name` longtext NOT NULL,
  `color` varchar(50) DEFAULT NULL,
  `product_id` int NOT NULL,
  PRIMARY KEY (`image_id`),
  KEY `FK_product_image_product` (`product_id`),
  CONSTRAINT `FK_product_image_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_image`
--

LOCK TABLES `product_image` WRITE;
/*!40000 ALTER TABLE `product_image` DISABLE KEYS */;
INSERT INTO `product_image` VALUES (47,'http://res.cloudinary.com/padshop/image/upload/v1656964629/hrngdkmjofapen1zakdd.jpg','trắng',16),(48,'http://res.cloudinary.com/padshop/image/upload/v1656964631/stf7cv4mri5wi8nz1hcx.jpg','trắng',16),(49,'http://res.cloudinary.com/padshop/image/upload/v1656964633/pmrrztvlmug8lh9aceyk.jpg','xanh',16),(50,'http://res.cloudinary.com/padshop/image/upload/v1656964635/sbyqnti4hmzsospymcjy.jpg','xanh',16),(51,'http://res.cloudinary.com/padshop/image/upload/v1657048617/ymw7eicwcw3vexu8cago.jpg','trắng',17),(52,'http://res.cloudinary.com/padshop/image/upload/v1657048619/kf5wcasjk09zspnh2wyc.jpg','trắng',17),(53,'http://res.cloudinary.com/padshop/image/upload/v1657048621/ruyrq13pqawaaapbauzt.jpg','xám',17),(54,'http://res.cloudinary.com/padshop/image/upload/v1657048623/tgyildn5ncduv63l82gs.jpg','xám',17),(55,'http://res.cloudinary.com/padshop/image/upload/v1657048767/eq9w86y9rsmhrspfmpkr.jpg','be',18),(56,'http://res.cloudinary.com/padshop/image/upload/v1657048770/ugnrmfmdcbwmopxzziaa.jpg','be',18),(57,'http://res.cloudinary.com/padshop/image/upload/v1657048772/zxknshhg9t4fd2c4eqtg.jpg','be',18),(58,'http://res.cloudinary.com/padshop/image/upload/v1657048774/ontkrc89qychl0dhpxqc.jpg','xanh dương',18),(59,'http://res.cloudinary.com/padshop/image/upload/v1657048776/ovx2iigknx4iufjzyqoi.jpg','xanh dương',18),(60,'http://res.cloudinary.com/padshop/image/upload/v1657048779/fpp1ow5ojzccngcckwyp.jpg','xanh dương',18),(61,'http://res.cloudinary.com/padshop/image/upload/v1657049343/l0dcpm4f4mwxmylm2ka4.jpg','trắng',19),(62,'http://res.cloudinary.com/padshop/image/upload/v1657049346/xmfbbj9efwr9hveij0q9.jpg','trắng',19),(63,'http://res.cloudinary.com/padshop/image/upload/v1657049348/ln02djzevohwie9zvnfh.jpg','nâu',19),(64,'http://res.cloudinary.com/padshop/image/upload/v1657049350/i34rnieafosw45brvkkp.jpg','nâu',19),(65,'http://res.cloudinary.com/padshop/image/upload/v1657049489/qeurj2jtxvauo5by2f7l.jpg',NULL,20),(66,'http://res.cloudinary.com/padshop/image/upload/v1657049491/m3myat3omgabl3v03eam.jpg',NULL,20),(67,'http://res.cloudinary.com/padshop/image/upload/v1657049493/yexwgltegy9uejgr0ock.jpg',NULL,20),(68,'http://res.cloudinary.com/padshop/image/upload/v1657049617/jntn29q44ipbjynf6psf.jpg',NULL,21),(69,'http://res.cloudinary.com/padshop/image/upload/v1657049619/qtcs1y1hoadkxofkxjml.jpg',NULL,21),(70,'http://res.cloudinary.com/padshop/image/upload/v1657049621/fb3qq9tdyg2gxqtxxfct.jpg',NULL,21),(71,'http://res.cloudinary.com/padshop/image/upload/v1657049623/cccbx5lmcaxo6aaqagjz.jpg',NULL,21),(72,'http://res.cloudinary.com/padshop/image/upload/v1657049827/iamkccaqg2hy6tva9g1p.jpg','be',22),(73,'http://res.cloudinary.com/padshop/image/upload/v1657049829/mfjuxiiuevr1n8qacac3.jpg','be',22),(74,'http://res.cloudinary.com/padshop/image/upload/v1657049831/pk9s7hqspyjrsqktnl6k.jpg','be',22),(75,'http://res.cloudinary.com/padshop/image/upload/v1657049833/nrnwfobl73sz6pnfa9ty.jpg','đen',22),(76,'http://res.cloudinary.com/padshop/image/upload/v1657049835/f4kzj2brz8tfx0coerzo.jpg','đen',22),(77,'http://res.cloudinary.com/padshop/image/upload/v1657049837/bu4tbgwozvlb4vtcyrad.jpg','đen',22),(78,'http://res.cloudinary.com/padshop/image/upload/v1657049839/s35g4ezfpi1pwzxa1p3q.jpg','trắng',22),(79,'http://res.cloudinary.com/padshop/image/upload/v1657049972/kisbbh4lc1wxm1ez7s03.jpg','đen',23),(80,'http://res.cloudinary.com/padshop/image/upload/v1657049974/j1kobttjpwhllsreflfi.jpg','đen',23),(81,'http://res.cloudinary.com/padshop/image/upload/v1657049976/ak752zkdkg7jzhwuw7vr.jpg','xanh dương',23),(82,'http://res.cloudinary.com/padshop/image/upload/v1657049978/ivblsxylpmfd9rjhorae.jpg','xanh dương',23),(83,'http://res.cloudinary.com/padshop/image/upload/v1657050128/flb5bdqccd5zrntgrlqd.jpg','trắng',24),(84,'http://res.cloudinary.com/padshop/image/upload/v1657050130/uy4iprwypetvvsc4ucxg.jpg','trắng',24),(85,'http://res.cloudinary.com/padshop/image/upload/v1657050132/uvbdlnlm3f2twwqueqml.jpg','đen',24),(86,'http://res.cloudinary.com/padshop/image/upload/v1657050134/pdetrdw2569xd83ptg9e.jpg','đen',24),(87,'http://res.cloudinary.com/padshop/image/upload/v1657050205/qagsnqwxekdjv1fwzoxq.jpg',NULL,25),(88,'http://res.cloudinary.com/padshop/image/upload/v1657050207/zoev80gothyr2phfxcxs.jpg',NULL,25),(89,'http://res.cloudinary.com/padshop/image/upload/v1657050209/ihb4qet8xlqby7r3blng.jpg',NULL,25),(90,'http://res.cloudinary.com/padshop/image/upload/v1657050211/wkus2odg7r3ummlcrtf1.jpg',NULL,25),(92,'http://res.cloudinary.com/padshop/image/upload/v1683250598/echlle08oa4ruw2mzq7t.jpg',NULL,27);
/*!40000 ALTER TABLE `product_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_quantity`
--

DROP TABLE IF EXISTS `product_quantity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_quantity` (
  `quantity_id` int NOT NULL AUTO_INCREMENT,
  `size` varchar(50) NOT NULL,
  `quantity` int NOT NULL,
  `product_id` int NOT NULL,
  `color` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`quantity_id`),
  KEY `FK_product_quantity_product_image` (`product_id`),
  CONSTRAINT `FK_product_quantity_product_image` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_quantity`
--

LOCK TABLES `product_quantity` WRITE;
/*!40000 ALTER TABLE `product_quantity` DISABLE KEYS */;
INSERT INTO `product_quantity` VALUES (79,'m',49,16,'trắng'),(80,'l',9,16,'trắng'),(81,'xl',0,16,'trắng'),(82,'m',0,16,'xanh'),(83,'l',44,16,'xanh'),(84,'xl',45,16,'xanh'),(85,'m',98,17,'trắng'),(86,'l',98,17,'trắng'),(87,'xl',100,17,'trắng'),(88,'m',100,17,'xám'),(89,'l',100,17,'xám'),(90,'xl',100,17,'xám'),(91,'m',100,18,'be'),(92,'l',100,18,'be'),(93,'xl',100,18,'be'),(94,'m',100,18,'xanh dương'),(95,'l',100,18,'xanh dương'),(96,'xl',100,18,'xanh dương'),(97,'m',100,19,'trắng'),(98,'l',100,19,'trắng'),(99,'xl',100,19,'trắng'),(100,'m',100,19,'nâu'),(101,'l',94,19,'nâu'),(102,'xl',99,19,'nâu'),(103,'m',50,20,NULL),(104,'l',50,20,NULL),(105,'xl',50,20,NULL),(106,'m',150,21,NULL),(107,'l',2,21,NULL),(108,'xl',55,21,NULL),(109,'m',500,22,'be'),(110,'l',500,22,'be'),(111,'xl',502,22,'be'),(112,'m',5,22,'đen'),(113,'l',5,22,'đen'),(114,'xl',5,22,'đen'),(115,'m',50,22,'trắng'),(116,'l',50,22,'trắng'),(117,'xl',50,22,'trắng'),(118,'m',50,23,'đen'),(119,'l',33,23,'đen'),(120,'xl',50,23,'đen'),(121,'m',50,23,'xanh dương'),(122,'l',50,23,'xanh dương'),(123,'xl',50,23,'xanh dương'),(124,'m',50,24,'trắng'),(125,'l',45,24,'trắng'),(126,'xl',50,24,'trắng'),(127,'m',50,24,'đên'),(128,'l',36,24,'đên'),(129,'xl',48,24,'đên'),(130,'m',43,25,NULL),(131,'l',26,25,NULL),(132,'xl',0,25,NULL),(135,'M',500,27,NULL),(136,'L',50,27,NULL),(137,'XL',49,27,NULL);
/*!40000 ALTER TABLE `product_quantity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promotion`
--

DROP TABLE IF EXISTS `promotion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `promotion` (
  `promotion_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `img_name` longtext NOT NULL,
  `discount` int DEFAULT NULL,
  PRIMARY KEY (`promotion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promotion`
--

LOCK TABLES `promotion` WRITE;
/*!40000 ALTER TABLE `promotion` DISABLE KEYS */;
INSERT INTO `promotion` VALUES (3,'THUY','Thuy','http://res.cloudinary.com/padshop/image/upload/v1708966181/csvrxurdm8xmzlzdo1nn.png',10),(4,'Sale Mùa Hè','hehehe','http://res.cloudinary.com/padshop/image/upload/v1708966209/bbvimdviltud5glo6htv.png',100);
/*!40000 ALTER TABLE `promotion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_tblrental_car`
--

DROP TABLE IF EXISTS `temp_tblrental_car`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `temp_tblrental_car` (
  `id` int NOT NULL DEFAULT '0',
  `unit_price` int NOT NULL,
  `rent_date` date NOT NULL,
  `return_date` date NOT NULL,
  `id_car` int NOT NULL,
  `id_contract` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_tblrental_car`
--

LOCK TABLES `temp_tblrental_car` WRITE;
/*!40000 ALTER TABLE `temp_tblrental_car` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp_tblrental_car` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_address`
--

DROP TABLE IF EXISTS `user_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_address` (
  `address_id` int NOT NULL AUTO_INCREMENT,
  `province` varchar(50) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  `ward` varchar(50) DEFAULT NULL,
  `detail` varchar(50) DEFAULT NULL,
  `user_id` int NOT NULL,
  `receiver_name` varchar(50) NOT NULL,
  `phone_number` char(10) DEFAULT NULL,
  PRIMARY KEY (`address_id`),
  KEY `FK_user_address_usertbl` (`user_id`),
  CONSTRAINT `FK_user_address_usertbl` FOREIGN KEY (`user_id`) REFERENCES `usertbl` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_address`
--

LOCK TABLES `user_address` WRITE;
/*!40000 ALTER TABLE `user_address` DISABLE KEYS */;
INSERT INTO `user_address` VALUES (1,'Thành phố Hà Nội','Quận Ba Đình','Phường Phúc Xá','PTIT A3',1,'Duc Pham','0354373737'),(2,'Tỉnh Yên Bái','Huyện Trấn Yên','Xã Hưng Khánh','Vân Tảo',3,'Test Nhom','0123456789'),(3,'Tỉnh Cao Bằng','Huyện Quảng Hòa','Xã Cách Linh','PTIT-A2',7,'Dat Hoang','0367436886'),(4,'Tỉnh Lai Châu','Huyện Phong Thổ','Xã Pa Vây Sử','Số nhà 303',8,'Trang Trần','0347477783'),(5,'Tỉnh Lạng Sơn','Huyện Bình Gia','Xã Thiện Long','thôn Tè',9,'Văn Tuấn','0343345842'),(27,'Tỉnh Hà Giang','Huyện Đồng Văn','Xã Vần Chải','BKAV',20,'Van Cuong','3333333333'),(30,'Tỉnh Bắc Giang','Huyện Lục Ngạn','Xã Hồng Giang','PTIT-A2',24,'Admin','0364743279'),(32,'Thành phố Hà Nội','Quận Hà Đông','Phường Mộ Lao','PTIT-A3',24,'Đức Admin','0364743279'),(33,'Tỉnh Bắc Giang','Huyện Lục Nam','Xã Nghĩa Phương','thôn Phượng Hoàng',21,'Anh Duc','0364743279'),(34,'Tỉnh Hà Giang','Huyện Yên Minh','Xã Du Già','PTIT.',21,'Phuong Thuy','0333333333'),(1034,'Thành phố Hà Nội','Quận Hà Đông','Phường Biên Giang','BKAV',21,'Van Cuong','0364743279'),(1035,'Tỉnh Vĩnh Phúc','Huyện Lập Thạch','Xã Xuân Lôi','PTIT A2',25,'Duc Pham','0364743279'),(1036,'Thành phố Hà Nội','Huyện Thanh Trì','Xã Duyên Hà','PTIT.',21,'Anh Tu','3433333333'),(1037,'Tỉnh Tuyên Quang','Thành phố Tuyên Quang','Xã Kim Phú','PTIT A3',26,'Pham Anh Duc','0364743279');
/*!40000 ALTER TABLE `user_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usertbl`
--

DROP TABLE IF EXISTS `usertbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usertbl` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` longtext,
  `role` varchar(10) NOT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `point` int DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usertbl`
--

LOCK TABLES `usertbl` WRITE;
/*!40000 ALTER TABLE `usertbl` DISABLE KEYS */;
INSERT INTO `usertbl` VALUES (1,'nptpad','$2a$10$FKES3hsQhYBggNw4dQtr7exHfmKK8L0dHdfkEypeLIVQ8GdAGHKsu','ROLE_USER','Duc Pham','0354373737',0,'phamduc10920@gmail.com'),(3,'nhom2th','$2a$10$I.TAayYzc1KjF4tXC/tffuoxZbAi1RGplWjQTelaKV06HzwChWaJS','ROLE_USER','Test Nhom','0123456789',0,'phantruongvu2001@gmail.com'),(7,'dat172001','$2a$10$Jgep/8JAlXj2ahSh7D6DpeNwKVUWSeqpDZGCWZoCT1PstujWy0.xW','ROLE_USER','Dat Hoang','0367436886',0,'datht@gmail.com'),(8,'user111','$2a$10$m443hRPqxUu0QZtBwI.01.vKsTIrYhZ0S/6GLLqT9/hJzcfRV8Qli','ROLE_USER','Trang Trần','0347477783',0,'trangtran@gmail.com'),(9,'user112','$2a$10$fbaPLr8iUhiQG25tPcdiXeiXGOfZ2YDrV8hGcFgnWzbPF2.PMNeDq','ROLE_USER','Văn Tuấn','0343345842',0,'vantuan@gmail.com'),(20,'nvc087','$2a$10$jk1sj78wlwWeQ4UhGTD4s.1FDXL.sh8uy7vThC1nWoZXPW1BCRABS','ROLE_USER','Van Cuong','0999999999',0,''),(21,'pad1092','$2a$10$swXuS92Y3RsWaK6Eo2APEeSY5wy.1cLpk8pdD5VHtrNNalvwr5mXe','ROLE_USER','Anh Đức','0364743278',0,''),(24,'manager','$2a$10$40GUv2jYNJ8ZovVqGsvje.DN4JXUaCKat7Jnih9BnqI0sT3vCsSz.','ROLE_ADMIN','vang cuong','8888888888',0,''),(25,'pad1093','$2a$10$aV.yeBeqw6XTyPjdBqY.keDd0qfmL3d4o92wmet6GZKkfUvoztDxS','ROLE_USER','Duc Pham','0364743279',0,''),(26,'pad1094','$2a$10$1jl0lsZWQ8Q1SNFm.yUKWeJHqa5RzQLfL4nZ7O4CqxeO/.Ny8iD.a','ROLE_USER','Pham Anh Duc','0364743279',0,'');
/*!40000 ALTER TABLE `usertbl` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-02-26 23:53:39

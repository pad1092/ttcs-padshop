var currDate = new Date()
var currMonth = currDate.getMonth()
var currYear = currDate.getFullYear()
var pieChart = lineChart = null;
$(document).ready(function () {
    $('#month-revenue-title').text(`Doanh Thu Năm ${currYear}`)
    setRevenueDate()
    getYearRevenueData();
})

function setRevenueDate(){
    $('#revenue-total-date').text(`${currMonth+1}/${currYear}`)
    getMonthData();
}
function prevMonth(){
    currMonth -= 1;
    currDate = new Date(currYear, currMonth)
    currMonth = currDate.getMonth();
    currYear = currDate.getFullYear();
    setRevenueDate();
}
function nextMonth(){
    currMonth += 1;
    currDate = new Date(currYear, currMonth)
    currMonth = currDate.getMonth();
    currYear = currDate.getFullYear();
    setRevenueDate();
}
function getMonthData(){
    let url = API_URL + `/month-revenue/${currMonth+1}-${currYear}`
    $.ajax({
        url: url,
        type: 'GET',

    }).done(function(response){
        prepareDataRevenue(response, 'total')
    })
}
function getYearRevenueData(){
    let url = API_URL +`/year-revenue/${currMonth+1}-${currYear}`
    $.ajax({
        url: url,
        type: 'GET'
    }).done(function (response){
        prepareDataRevenue(response, 'month')
    })
}
function prepareDataRevenue(dataJson, type){
    const date = new Date(currYear, currMonth+1, 0);
    const numOfDays = date.getDate();

    if (type === 'total'){
        dataJson = insertMissingData(dataJson, numOfDays)
        dataJson.sort(function (a, b) {
            return a.day - b.day
        })
        showRevenueTotalChart(dataJson)
    }
    else if (type === 'month'){
        dataJson = insertMissingData(dataJson, currMonth+1)
        dataJson.sort(function (a, b) {
            return a.day - b.day
        })
        showMonthRevenueChar(dataJson);
    }

    //sort by date ascending

}

function insertMissingData(dataJson, numOfDays){
    for (var i = 1;  i <= numOfDays; i++){
        var found = dataJson.some(data => data.day === i);
        if (!found) {
            dataJson.push({"total": 0, "day": i});
        }
    }
    return dataJson;
}

//display chart
function showRevenueTotalChart(dataJson) {
    if (lineChart != null){
        lineChart.destroy();
    }
    const canvas = document.getElementById('revenue-total-chart')
    var days = dataJson.map(obj => obj.day);
    var totals = dataJson.map(obj => obj.total);
    // Tạo biểu đồ đường sử dụng Chart.js
    var ctx = canvas.getContext('2d');

    let lineChartConfig = {
        type: 'line',
        data: {
            labels: days,
            datasets: [{
                label: 'Doanh thu',
                data: totals,
                borderWidth: 2,
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                borderColor: 'rgba(54, 162, 235, 1)',
                fill: true,
                tension: 0.5
            }]
        },
        options: {
            responsive: true,
            scales: {
                x: {
                    display: true,
                    title: {
                        display: true,
                        color: 'rgba(0, 0, 0, 0.8)',
                        text: 'Ngày'
                    }
                },
                y: {
                    display: true,
                    title: {
                        display: true,
                        color: 'rgba(0, 0, 0, 0.8)',
                        text: 'Tổng doanh thu'

                    }
                }
            },
        }
    }
    lineChart = new Chart(ctx, lineChartConfig);
}
function showMonthRevenueChar(dataJson){
    console.log(dataJson)
    const canvas = document.getElementById('month-revenue-canvas')
    var ctx = canvas.getContext('2d');

    if (pieChart != null){
        pieChart.destroy();
    }
    var labels = dataJson.map(obj => 'Tháng ' + obj.day);
    var colors = generateColors(dataJson.length);
    var total = dataJson.map(obj => obj.total);
    let config = {
        type: 'doughnut',
        data: {
            labels: labels,
            datasets: [{
                label: "Doanh thu",
                data: total,
                backgroundColor: colors,
                hoverOffset: 4
            }]
        }
    }
    pieChart = new Chart(ctx, config)
}

function generateColors(length) {
    const colors = [];
    for (let i = 0; i < length; i++) {
        const hue = (i * 360) / length;
        const color = `hsl(${hue}, 50%, 50%)`;
        colors.push(color);
    }
    return colors;
}
$(document).ready(function (){
    callAPI();
});
function callAPI(){
    let url = API_URL + '/overview-revenue';
    $.get(url, function (response){
        console.log(response)
        displayData(response);
    })
}

function displayData(data){
    let percentHtml = null;

    let ovvCurrVal = data.total_curr;
    let ovvPreVal = data.total_pre
    let ovvPercent = Math.round(100*Math.abs(ovvCurrVal-ovvPreVal) / ((ovvCurrVal+ovvPreVal)/2));

    if (ovvCurrVal > ovvPreVal){
        percentHtml = `
        <div class="d-flex flex-column text-success">
            <span>+${ovvPercent}%</span><i class="fa-solid fa-sort-up"></i>
        </div>
        `
    }
    else{
        percentHtml = `
        <div class="d-flex flex-column text-danger">
            <i class="fa-solid fa-sort-down"></i><span>-${ovvPercent}%</span>
        </div>
        `
    }
    $('#ovv-rvn-pct').html(percentHtml);
    $("#ovv-rvn-curr-val").text(ovvCurrVal.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' }));
    $("#ovv-rvn-pre-val").text(ovvPreVal.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' }));

    let ordCurrVal = data.order_curr;
    let ordPreVal = data.order_pre;
    let ordPct = Math.round(100*Math.abs(ordPreVal-ordCurrVal) / ((ordPreVal+ordCurrVal)/2));
    if (ordCurrVal > ordPreVal){
        percentHtml = `
        <div class="d-flex flex-column text-success">
            <span>+${ordPct}%</span><i class="fa-solid fa-sort-up"></i>
        </div>
        `
    }
    else{
        percentHtml = `
        <div class="d-flex flex-column text-danger">
            <i class="fa-solid fa-sort-down"></i><span>-${ordPct}%</span>
        </div>
        `
    }
    $('#ord-rvn-pct').html(percentHtml);
    $('#ord-rvn-curr-val').text(ordCurrVal);
    $('#ord-rvn-pre-val').text(ordPreVal);
}

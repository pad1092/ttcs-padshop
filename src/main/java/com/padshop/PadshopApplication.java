package com.padshop;

import com.padshop.entities.*;
import com.padshop.repositories.*;
import com.padshop.services.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PadshopApplication {

    public static void main(String[] args) {
        SpringApplication.run(PadshopApplication.class, args);
    }

}

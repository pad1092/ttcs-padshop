package com.padshop.repositories;

import com.padshop.dto.NetRevenueDTO;
import com.padshop.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    Order findOrderByOrderId(Long id);

    List<Order> findAllByUserOrderUserId(Long userId);

    List<Order> findAllByStatusEquals(int status);

    @Query(value="SELECT * from\n" +
            "\t(select coalesce(sum(pay_total), 0) as total_curr from ordertbl \n" +
            "\t\twhere year(buying_date) = year(CURRENT_DATE) and month(buying_date) = month(CURRENT_DATE)\n" +
            "\t) as curr_month,\n" +
            "    (select coalesce(sum(pay_total), 0) as total_pre from ordertbl \n" +
            "\t\twhere year(buying_date) = year(CURRENT_DATE) and month(buying_date) = month(CURRENT_DATE)-1\n" +
            "\t) as prev_month,\n" +
            "    (select count(*) as order_curr from ordertbl\n" +
            "\t\twhere year(buying_date) = year(CURRENT_DATE) and month(buying_date) = month(CURRENT_DATE)\n" +
            "\t) as order_curr,\n" +
            "    (select count(*) as order_prev from ordertbl\n" +
            "\t\twhere year(buying_date) = year(CURRENT_DATE) and month(buying_date) = month(CURRENT_DATE)-1\n" +
            "\t) as order_prev;", nativeQuery = true)
    List<Object[]> revenueOverview();

    int countByStatus(int status);

    @Query(value = "SELECT DATE(o.buying_date) AS date, SUM(o.pay_total) AS revenue " +
            "FROM ordertbl o " +
            "WHERE o.buying_date BETWEEN :start AND :end " +
            "GROUP BY DATE(o.buying_date) " +
            "ORDER BY DATE(o.buying_date)", nativeQuery = true)
    List<Object[]> findRevenueByDay(@Param("start") LocalDate start, @Param("end") LocalDate end);

    //  get  total revenue
    @Query(value="select * from (select SUM(pay_total) as total , DAY(buying_date) as day from ordertbl\n" +
            "where MONTH(buying_date) = :month and status > 0 and YEAR(buying_date) = :year \n" +
            "group by (buying_date) order by (buying_date)) as s", nativeQuery = true)
    List<Object[]> getTotalRevenue(@Param("month") int month, @Param("year") int year);

    @Query(value = "select * from (SELECT SUM(pay_total) as total , month(buying_date) as month from ordertbl\n" +
            "where MONTH(buying_date) <= :m and status > 0 and YEAR(buying_date) = :y\n" +
            "group by Month(buying_date)\n" +
            "order by Month(buying_date)) as s", nativeQuery = true)
    List<Object[]> getMonthRevenue(@Param("m") int m, @Param("y") int y);
}
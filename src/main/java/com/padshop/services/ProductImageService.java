package com.padshop.services;

import com.padshop.dto.ProductImageDTO;
import com.padshop.entities.Product;
import com.padshop.entities.ProductImage;
import com.padshop.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductImageService {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    StorageFileService storageFileService;

    // save product Image
    public void saveProductImage(ProductImageDTO productImageDTO) {
        List<ProductImage> productImages = new ArrayList<>();
        int listImgSize = productImageDTO.getImageFile().length;
        Product product = productRepository.findProductByProductId(productImageDTO.getProductId());
        for (int i = 0; i < listImgSize; i++) {
            ProductImage productImage = new ProductImage();
            if (productImageDTO.getColors() != null) {
                String color = productImageDTO.getColors().get(i);
                productImage.setColor(color);
            }
            MultipartFile file = productImageDTO.getImageFile()[i];
            String url = storageFileService.storageFile(file);
            productImage.setName(url);
            productImage.setProductImage(product);
            productImages.add(productImage);

        }
        product.setProductImages(productImages);
        productRepository.saveAndFlush(product);
    }
}

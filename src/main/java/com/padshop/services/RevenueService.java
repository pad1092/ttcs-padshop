package com.padshop.services;

import com.padshop.dto.NetRevenueDTO;
import com.padshop.dto.OverviewRevenueDTO;
import com.padshop.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
public class RevenueService {
    @Autowired
    private OrderRepository orderRepository;

    // get total revenue of a month in year
    public List<NetRevenueDTO> getTotalRevenue(int month, int year){
        List<NetRevenueDTO> dtoList = new ArrayList<>();
        orderRepository.getTotalRevenue(month, year).forEach(revenue -> {
            BigDecimal total = (BigDecimal) revenue[0];
            int day = (int)revenue[1];
            dtoList.add(new NetRevenueDTO(total, day));
        });
        return dtoList;
    }
    public List<NetRevenueDTO> getMonthRevenue(int m, int y){
        List<NetRevenueDTO> dtoList = new ArrayList<>();
        orderRepository.getMonthRevenue(m, y).forEach(revenue -> {
            BigDecimal total = (BigDecimal) revenue[0];
            int day = (int)revenue[1];
            dtoList.add(new NetRevenueDTO(total, day));
        });
        return dtoList;
    }

    public OverviewRevenueDTO getRevenueOverview(){
        List<Object[]> obj = orderRepository.revenueOverview();
        BigDecimal totalCurr = (BigDecimal)obj.get(0)[0];
        BigDecimal totalPrev = (BigDecimal)obj.get(0)[1];
        BigInteger orderCurr = (BigInteger)obj.get(0)[2];
        BigInteger orderPrev = (BigInteger)obj.get(0)[3];

        return new OverviewRevenueDTO(totalCurr, totalPrev, orderCurr, orderPrev);
    }
}

package com.padshop.services;

import com.padshop.dto.ProductDTO;
import com.padshop.entities.Product;
import com.padshop.entities.Promotion;
import com.padshop.repositories.ProductRepository;
import com.padshop.repositories.PromotionRepository;
import com.padshop.util.Paged;
import com.padshop.util.Paging;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private PromotionRepository promotionRepository;
    @Autowired
    private CalculateService calculateService;
    @Autowired
    private ModelMapper mapper;

    // add product
    public Long addProduct(ProductDTO productDTO) {
        Product product = toEntity(productDTO);
        productRepository.save(product);
        Long productId = product.getProductId();
        product.setCode(product.getCode() + productId);
        productRepository.saveAndFlush(product);
        return productId;
    }

    public void removeProduct(Long id) {
        productRepository.deleteById(id);
    }

    public void updateProduct(Long productId, Product data) {
        Product product = productRepository.findProductByProductId(productId);
        product.setName(data.getName());
        product.setPrice(data.getPrice());
        data.getProductQuantities().forEach(quantity -> {
            quantity.setProductQuantity(product);
        });
        product.setProductQuantities(data.getProductQuantities());
        productRepository.saveAndFlush(product);
    }

    public List<ProductDTO> getNewestProduct() {
        List<Product> productList = productRepository.findTop8ByProductPromotionIsNullOrderByProductIdDesc();
        List<ProductDTO> productDTOS = listEntityToListDTO(productList);
        return productDTOS;
    }

    public List<Product> getListProduct() {
        return productRepository.findAll();
    }

    public List<ProductDTO> getSaleProduct() {
        List<Product> productList = productRepository.findTop8ByProductPromotionNotNull();
        List<ProductDTO> productDTOS = listEntityToListDTO(productList);
        return productDTOS;
    }

    public Product getProductById(Long productId) {
        return productRepository.findProductByProductId(productId);
    }

    public ProductDTO getProductDTOById(Long productId) {
        Product product = productRepository.findProductByProductId(productId);
        ProductDTO productDTO = toDTO(product);

        // set price discount
        if (product.getProductPromotion() == null) {
            productDTO.setPromote(false);
        } else {
            productDTO.setPromote(true);
            productDTO.setDiscount(product.getProductPromotion().getDiscount());
            productDTO.setDiscountPrice(product.getPrice() - product.getPrice() * product.getProductPromotion().getDiscount() / 100);
        }

        // set is no color;
        product.getProductQuantities().forEach(data -> {
            if (data.getColor() == null) {
                productDTO.setNoColor(true);
                return;
            }
        });
        return productDTO;
    }

    // get list of product not in promotion
    public List<ProductDTO> getProductsNotDiscount() {
        return convertListPNIPtoProductDTO(productRepository.findAllByProductPromotionNull());
    }

    public List<ProductDTO> getListProductByCate(String cate) {
        if (cate.equalsIgnoreCase("ALL")) {
            cate = "%";
        } else {
            cate += "%";
        }
        return getListDTO(productRepository.findAllByCodeLike(cate));
    }

    // remove promotion
    public void removePromotionofListProduct(List<Long> listProductId) {
        for (Long productId : listProductId) {
            Product product = productRepository.findProductByProductId(productId);
            product.setProductPromotion(null);
            productRepository.save(product);
        }
    }

    public void addPromotionOfListProduct(List<Long> listProductId) {
        // last index is promotionId;
        Promotion promotion = promotionRepository.findByPromotionId(listProductId.get(listProductId.size() - 1));
        for (int i = 0; i < listProductId.size() - 1; i++) {
            Product product = productRepository.findProductByProductId(listProductId.get(i));
            product.setProductPromotion(promotion);
            productRepository.saveAndFlush(product);
        }
    }

    //search
    public List<ProductDTO> searchProduct(String keyword) {
        return getListDTO(productRepository.findAllByNameContainingOrCodeContaining(keyword, keyword));
    }

    public List<ProductDTO> searchPIP(String name, Long promotionId) {
        return convertListPIPtoProductDTO(productRepository.getProductIPByName(name, promotionId));
    }

    public List<ProductDTO> searchPNIP(String name) {
        return convertListPNIPtoProductDTO(productRepository.getProductNIPByName(name));
    }

    // search by name
    // paging
    public Paged<Product> searchProductGetPage(int pageNumber, String keyword, int sortBy) {
        // product each page
        int size = 4;
        PageRequest pageRequest = PageRequest.of(pageNumber - 1, size, Sort.by("productId").descending());
        Page<Product> productPage = productRepository.findAllByNameContaining(keyword, pageRequest);
        return new Paged<>(productPage, Paging.of(productPage.getTotalPages(), pageNumber, size));
    }

    public Paged<Product> cateProductGetPage(int pageNumber, String code) {
        int size = 4;
        PageRequest pageRequest = PageRequest.of(pageNumber - 1, size, Sort.by("productId").ascending());
        code += "%";
        Page<Product> productPage = productRepository.findAllByCodeLike(code, pageRequest);
        System.out.println(productPage.getTotalElements());
        return new Paged<>(productPage, Paging.of(productPage.getTotalPages(), pageNumber, size));
    }

    public Paged<Product> getPageProductByPromotion(int pageNumber, Long promotionId) {
        int size = 4;
        PageRequest pageRequest = PageRequest.of(pageNumber - 1, size, Sort.by("productId").ascending());

        Page<Product> productPage = productRepository.findAllByProductPromotionPromotionId(promotionId, pageRequest);
        System.out.println(productPage.getTotalElements());
        return new Paged<>(productPage, Paging.of(productPage.getTotalPages(), pageNumber, size));
    }

    public List<ProductDTO> getListDTO(List<Product> products) {
        List<ProductDTO> productDTOList = new ArrayList<>();
        for (Product product : products) {
            ProductDTO productDTO = toDTO(product);
            productDTO.setImageName(product.getProductImages().get(0).getName());

            if (product.getProductPromotion() != null) {
                productDTO.setDiscount(product.getProductPromotion().getDiscount());
                int discountPrice = product.getPrice() - product.getPrice() * product.getProductPromotion().getDiscount() / 100;
                productDTO.setDisPriceCurr(calculateService.numberToCurrency(discountPrice));
            }

            productDTO.setName(product.getName() + ' ' + product.getCode());
            productDTO.setPriceCurr(calculateService.numberToCurrency(product.getPrice()));
            productDTO.setPromote(product.getProductPromotion() != null);

            productDTOList.add(productDTO);

        }
        return productDTOList;
    }

    //commom
    public List<ProductDTO> convertListPNIPtoProductDTO(List<Product> products) {
        List<ProductDTO> res = new ArrayList<>();
        products.forEach(product -> {
            ProductDTO productDTO = new ProductDTO();
            productDTO.setProductId(product.getProductId());
            productDTO.setImageName(product.getProductImages().get(0).getName());
            productDTO.setName(product.getName() + " " + product.getCode().toUpperCase());
            productDTO.setPrice(product.getPrice());
            res.add(productDTO);
        });
        return res;
    }

    public List<ProductDTO> convertListPIPtoProductDTO(List<Product> products) {
        List<ProductDTO> res = new ArrayList<>();
        products.forEach(product -> {
            ProductDTO productDTO = new ProductDTO();
            productDTO.setName(product.getName() + " " + product.getCode());
            productDTO.setImageName(product.getProductImages().get(0).getName());
            productDTO.setProductId(product.getProductId());
            productDTO.setPrice(product.getPrice());
            productDTO.setDiscountPrice(product.getPrice() - product.getPrice() * product.getProductPromotion().getDiscount() / 100);
            res.add(productDTO);
        });
        return res;
    }

    public Long findProductIdBySlug(String slug){
        return productRepository.findProductIdBySlug(slug);
    }
    public String getSlugById(Long id){
        Product product = productRepository.getById(id);
        return toDTO(product).getSlug();
    }

    //mapper
    public ProductDTO toDTO(Product product) {
        ProductDTO productDTO =  mapper.map(product, ProductDTO.class);
        String productName = product.getName().trim() + " " + product.getCode().trim();
        String normalize = java.text.Normalizer.normalize(productName, Normalizer.Form.NFD);
        String slug = normalize.replaceAll("\\p{M}", "")
                                .replaceAll(" ", "-")
                                .toLowerCase();
        productDTO.setSlug(slug);
        return productDTO;
    }

    public Product toEntity(ProductDTO productDTO) {
        return mapper.map(productDTO, Product.class);
    }

    public List<ProductDTO> listEntityToListDTO(List<Product> productList) {
        List<ProductDTO> productDTOS = new ArrayList<>();
        productList.forEach(product -> {
            ProductDTO productDTO = toDTO(product);
            if (product.getProductImages().size() > 0) {
                productDTO.setImageName(product.getProductImages().get(0).getName());
            }
            if (product.getProductPromotion() != null) {
                int discountPrice = product.getPrice() - product.getPrice() * product.getProductPromotion().getDiscount() / 100;
                productDTO.setDisPriceCurr(calculateService.numberToCurrency(discountPrice));
            }
            productDTO.setPriceCurr(calculateService.numberToCurrency(product.getPrice()));
            productDTOS.add(productDTO);
        });
        return productDTOS;
    }
}

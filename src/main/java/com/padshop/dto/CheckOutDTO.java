package com.padshop.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
public class CheckOutDTO {
    private Long quantityId;
    private int quantity;
}

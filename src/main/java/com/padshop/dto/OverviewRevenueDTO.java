package com.padshop.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.BigInteger;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OverviewRevenueDTO {
    private BigDecimal total_curr;
    private BigDecimal total_pre;
    private BigInteger order_curr;
    private BigInteger order_pre;
}

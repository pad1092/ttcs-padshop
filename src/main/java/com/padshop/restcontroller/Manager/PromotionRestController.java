package com.padshop.restcontroller.Manager;

import com.padshop.dto.ProductDTO;
import com.padshop.dto.PromotionDTO;
import com.padshop.services.PromotionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("manage/api/v1")
public class PromotionRestController {
    @Autowired
    PromotionService service;

    @GetMapping("/promotion/{id}")
    public PromotionDTO getPromotion(@PathVariable("id") Long promotionId) {
        return service.getPromotionById(promotionId);
    }


    @GetMapping("/promotion/{id}/products")
    public List<ProductDTO> getProductList(@PathVariable("id") Long promotionId) {
        return service.getProductList(promotionId);
    }

}

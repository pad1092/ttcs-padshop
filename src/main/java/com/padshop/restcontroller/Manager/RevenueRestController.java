package com.padshop.restcontroller.Manager;

import com.padshop.dto.NetRevenueDTO;
import com.padshop.dto.OverviewRevenueDTO;
import com.padshop.repositories.OrderRepository;
import com.padshop.services.OrderService;
import com.padshop.services.RevenueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("manage/api/v1")
public class RevenueRestController {
    @Autowired
    RevenueService service;
    @Autowired
    OrderRepository orderRepository;

    @GetMapping("/month-revenue/{month}-{year}")
    public List<NetRevenueDTO> getTotalRevenue(@PathVariable("month") int month, @PathVariable("year") int year){
        return service.getTotalRevenue(month, year);
    }

    @GetMapping("/year-revenue/{m}-{y}")
    public List<NetRevenueDTO> getMonthRevenue(@PathVariable("m") int m, @PathVariable("y") int y){
        return service.getMonthRevenue(m, y);
    }
    @GetMapping("/overview-revenue")
    public Object getRevenueOverview(){
        return service.getRevenueOverview();
    }
}

package com.padshop.restcontroller.Product;

import com.padshop.dto.ProductQuantityDTO;
import com.padshop.services.ProductQuantityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ProductQuantityRestController {
    @Autowired
    ProductQuantityService service;

    @PostMapping("/product-quantity")
    public void addProductQty(@RequestBody List<ProductQuantityDTO> listQty) {
        service.saveProductQty(listQty);
    }
}

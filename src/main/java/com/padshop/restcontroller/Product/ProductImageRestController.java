package com.padshop.restcontroller.Product;

import com.padshop.dto.ProductImageDTO;
import com.padshop.services.ProductImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
public class ProductImageRestController {
    @Autowired
    ProductImageService service;

    @PostMapping("/product-image")
    public void addProductImage(@ModelAttribute ProductImageDTO productImage) {
        service.saveProductImage(productImage);
    }
}

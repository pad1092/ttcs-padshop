package com.padshop.restcontroller;

import com.padshop.dto.OrderDTO;
import com.padshop.dto.OrderItemDTO;
import com.padshop.repositories.OrderRepository;
import com.padshop.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class OrderRestController {
    @Autowired
    OrderService orderService;
    @Autowired
    OrderRepository orderRepository;

    @GetMapping("/revenue")
    public List<Object[]> getRevenueByDay() {
        LocalDate end = LocalDate.now();
        LocalDate start = end.minusDays(6);
        return orderRepository.findRevenueByDay(start, end);
    }


    @GetMapping("/order-pending")
    public List<OrderDTO> getListOrderPending() {
        return orderService.getListOrderPending();
    }

    @PostMapping("/order/{orderId}")
    public void updateOrder(@PathVariable("orderId") Long orderId, @RequestParam("action") int action) {
        orderService.updateOrderStatus(orderId, action);
    }

    @GetMapping("/order/{orderId}")
    public OrderDTO getListOrder(@PathVariable("orderId") Long id) {
        return orderService.getOrderDTOById(id);
    }

    @GetMapping("/order/{orderId}/items")
    public List<OrderItemDTO> getListOrderItem(@PathVariable("orderId") Long id) {
        return orderService.getListOrderItemByOrderId(id);
    }
}

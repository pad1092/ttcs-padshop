package com.padshop.controller;

import com.padshop.dto.CheckOutDTO;
import com.padshop.dto.OrderItemListDTO;
import com.padshop.entities.Order;
import com.padshop.services.OrderItemService;
import com.padshop.services.OrderService;
import com.padshop.services.ProductQuantityService;
import com.padshop.services.UserService;
import com.padshop.util.VNPay.VNPayService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@Slf4j
public class CheckOutController {
    @Autowired
    OrderItemService orderItemService;
    @Autowired
    UserService userService;
    @Autowired
    OrderService service;
    @Autowired
    ProductQuantityService productQuantityService;
    @Autowired
    VNPayService vnPayService;
    @Autowired
    private HttpServletRequest httpServletRequest;


    private final Logger LOGGER = LoggerFactory.getLogger(CheckOutController.class);
    @PostMapping("/product/checkout")
    public String productCheckout(@ModelAttribute("quantity") CheckOutDTO checkOutDTO, RedirectAttributes attributes) {
        attributes.addFlashAttribute("productCheckout", checkOutDTO);
        return "redirect:/checkout";
    }

    @PostMapping("/cart/checkout")
    public String cartCheckout(@ModelAttribute("quantity") OrderItemListDTO listOrderItemDTO, RedirectAttributes attributes) {
        List<CheckOutDTO> checkOutDTOList = service.orderItemToCheckOut(listOrderItemDTO.getOrderItemDTOList());
        attributes.addFlashAttribute("cartCheckout", checkOutDTOList);

        return "redirect:/checkout";
    }

    @GetMapping("/checkout")
    public String checkout(Model model) {
        CheckOutDTO checkOutDTO = (CheckOutDTO) model.getAttribute("productCheckout");
        List<CheckOutDTO> checkOutDTOS = (List<CheckOutDTO>) model.getAttribute("cartCheckout");

        OrderItemListDTO orderItems = new OrderItemListDTO();
        orderItemService.converToListItem(checkOutDTO, checkOutDTOS).forEach(orderItems::addOrderItem);

        model.addAttribute("orderItemList", orderItems);
        model.addAttribute("user", userService.getUserByLogged());
        model.addAttribute("order", new Order());
        return "checkout";
    }

    @PostMapping("/checkout")
    public String saveOrder(@ModelAttribute("orderItemList") OrderItemListDTO orderItems,
                            @ModelAttribute("order") Order order,
                            @RequestParam(value = "paymentMethod") String paymentMethod,
                            HttpServletRequest request) {
        if (paymentMethod.equalsIgnoreCase("COD")){
            service.saveOrder(orderItems.getOrderItemDTOList(), order, 0);
            productQuantityService.updateQuantity(orderItems.getOrderItemDTOList(), "sub");
            return "ordersuccess";
        }
        else if (paymentMethod.equalsIgnoreCase("VNPAY")){
            service.saveOrder(orderItems.getOrderItemDTOList(), order, -1);
            productQuantityService.updateQuantity(orderItems.getOrderItemDTOList(), "sub");
            int orderTotal = service.getOrderTotal(orderItems);
//            VPay
            String baseUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
            String vnpayUrl = vnPayService.createOrder(orderTotal, Math.toIntExact(order.getOrderId()), baseUrl);
            return "redirect:" + vnpayUrl;
        }
        return "";
    }

    @GetMapping("/vnpay-order-response")
    public String vnpayOrderReturn(HttpServletRequest request, Model model){
        int orderStatus = vnPayService.orderReturn(request);
        Long orderId = Long.valueOf((request.getParameter("vnp_OrderInfo").split(":")[1]));
        if (orderStatus == 1) {
            // action = 3, da thanh toan
            service.updatePayment(orderId);
            return "ordersuccess";
        }
        else {
            String orderInfo = request.getParameter("vnp_OrderInfo");
            String paymentTime = request.getParameter("vnp_PayDate");
            String transactionId = request.getParameter("vnp_TransactionNo");
            String totalPrice = request.getParameter("vnp_Amount");

            model.addAttribute("orderId", orderInfo);
            model.addAttribute("totalPrice", totalPrice);
            model.addAttribute("paymentTime", paymentTime);
            model.addAttribute("transactionId", transactionId);

            // action = 4, thanh toan that bai, xoa order
            service.updateOrderStatus(orderId, 4);
            return "orderfail";
        }
    }
}

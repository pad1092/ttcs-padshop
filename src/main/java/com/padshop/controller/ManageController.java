package com.padshop.controller;

import com.padshop.repositories.OrderRepository;
import com.padshop.services.OrderService;
import com.padshop.services.PromotionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ManageController {

    @Autowired
    PromotionService service;
    @Autowired
    OrderService orderService;
    @Autowired
    OrderRepository orderRepository;

    @GetMapping("/manage/promotion")
    public String getManagePromotion(Model model) {
        model.addAttribute("promotionList", service.getAllPromotion());
        return "manager/managepromotion";
    }

    @GetMapping("/manage")
    public String getManageHome(Model model) {
        model.addAttribute("pending", orderRepository.countByStatus(0));
        return "manager/managehome";
    }

    @GetMapping("/manage/product")
    public String getMangeProduct() {
        return "manager/manageproduct";
    }

    @GetMapping("/manage/order")
    public String getManageOrder(Model model) {
        model.addAttribute("orderList", orderService.getListOrderPending());
        return "manager/manageorder";
    }
    @GetMapping("/manage/revenue")
    public String getRevenue(){
        return "manager/managerevenue";
    }
}

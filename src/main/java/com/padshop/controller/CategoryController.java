package com.padshop.controller;

import com.padshop.entities.Product;
import com.padshop.services.ProductService;
import com.padshop.util.Paged;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CategoryController {

    @Autowired
    ProductService productService;

    @GetMapping("/ao-phong")
    public String getCateAP(@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber,
                            @RequestParam(value = "sortBy", required = false, defaultValue = "0") int sortBy,
                            Model model) {
        Paged<Product> productPaged = productService.cateProductGetPage(pageNumber, "APH");
        model.addAttribute("products", productPaged);
        model.addAttribute("productDTOs", productService.getListDTO(productPaged.getPage().toList()));
        model.addAttribute("title", "Áo phông");
        return "category";
    }

    @GetMapping("/ao-polo")
    public String getCateAPL(@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber,
                             @RequestParam(value = "sortBy", required = false, defaultValue = "0") int sortBy,
                             Model model) {
        Paged<Product> productPaged = productService.cateProductGetPage(pageNumber, "APL");
        model.addAttribute("products", productPaged);
        model.addAttribute("productDTOs", productService.getListDTO(productPaged.getPage().toList()));
        model.addAttribute("title", "Áo polo");

        return "category";
    }

    @GetMapping("/ao-so-mi")
    public String getCateASM(@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber,
                             @RequestParam(value = "sortBy", required = false, defaultValue = "0") int sortBy,
                             Model model) {
        Paged<Product> productPaged = productService.cateProductGetPage(pageNumber, "ASM");
        model.addAttribute("products", productPaged);
        model.addAttribute("productDTOs", productService.getListDTO(productPaged.getPage().toList()));
        model.addAttribute("title", "Áo sơ mi");

        return "category";
    }

    @GetMapping("/quan-jean")
    public String getCateQJ(@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber,
                            @RequestParam(value = "sortBy", required = false, defaultValue = "0") int sortBy,
                            Model model) {
        Paged<Product> productPaged = productService.cateProductGetPage(pageNumber, "QJE");
        model.addAttribute("products", productPaged);
        model.addAttribute("productDTOs", productService.getListDTO(productPaged.getPage().toList()));
        model.addAttribute("title", "Quần jean");

        return "category";
    }

    @GetMapping("/quan-au")
    public String getCateQA(@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber,
                            @RequestParam(value = "sortBy", required = false, defaultValue = "0") int sortBy,
                            Model model) {
        Paged<Product> productPaged = productService.cateProductGetPage(pageNumber, "QAU");
        model.addAttribute("products", productPaged);
        model.addAttribute("productDTOs", productService.getListDTO(productPaged.getPage().toList()));
        model.addAttribute("title", "Quần âu");

        return "category";
    }

    @GetMapping("/quan-jogger")
    public String getCateQJG(@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber,
                             @RequestParam(value = "sortBy", required = false, defaultValue = "0") int sortBy,
                             Model model) {
        Paged<Product> productPaged = productService.cateProductGetPage(pageNumber, "QJG");
        model.addAttribute("products", productPaged);
        model.addAttribute("productDTOs", productService.getListDTO(productPaged.getPage().toList()));
        model.addAttribute("title", "Quần jogger");

        return "category";
    }

    @GetMapping("/giay-dep")
    public String getCateGD(@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber,
                            @RequestParam(value = "sortBy", required = false, defaultValue = "0") int sortBy,
                            Model model) {
        Paged<Product> productPaged = productService.cateProductGetPage(pageNumber, "GDE");
        model.addAttribute("products", productPaged);
        model.addAttribute("productDTOs", productService.getListDTO(productPaged.getPage().toList()));
        model.addAttribute("title", "Giày dép");

        return "category";
    }

    @GetMapping("/tui-vi")
    public String getCateTV(@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber,
                            @RequestParam(value = "sortBy", required = false, defaultValue = "0") int sortBy,
                            Model model) {
        Paged<Product> productPaged = productService.cateProductGetPage(pageNumber, "TVI");
        model.addAttribute("products", productPaged);
        model.addAttribute("productDTOs", productService.getListDTO(productPaged.getPage().toList()));
        model.addAttribute("title", "Túi ví");

        return "category";
    }

    @GetMapping("/that-lung")
    public String getCateTL(@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber,
                            @RequestParam(value = "sortBy", required = false, defaultValue = "0") int sortBy,
                            Model model) {
        Paged<Product> productPaged = productService.cateProductGetPage(pageNumber, "TLU");
        model.addAttribute("products", productPaged);
        model.addAttribute("productDTOs", productService.getListDTO(productPaged.getPage().toList()));
        model.addAttribute("title", "Thắt lưng");

        return "category";
    }
}

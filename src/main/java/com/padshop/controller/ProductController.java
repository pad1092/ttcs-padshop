package com.padshop.controller;

import com.padshop.dto.CheckOutDTO;
import com.padshop.entities.Product;
import com.padshop.services.ProductQuantityService;
import com.padshop.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class ProductController {
    @Autowired
    ProductService productService;
    @Autowired
    ProductQuantityService productQuantityService;

    @GetMapping("/product/{slug}")
    public String getProduct(@PathVariable("slug") String slug, Model model) {
        Long productId = productService.findProductIdBySlug(slug);
        model.addAttribute("product", productService.getProductById(productId));
        model.addAttribute("productDTO", productService.getProductDTOById(productId));
        model.addAttribute("productQtyDTO", productQuantityService.getAllProductQty(productId));
        model.addAttribute("listSize", productQuantityService.listOfSize(productId));
        model.addAttribute("quantity", new CheckOutDTO());
        return "product";
    }
}

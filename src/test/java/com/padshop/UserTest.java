package com.padshop;

import com.padshop.dto.UserDTO;
import com.padshop.repositories.UserAddressRepository;
import com.padshop.repositories.UserRepository;
import com.padshop.services.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
public class UserTest {

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserAddressRepository userAddressRepository;
    @Autowired
    UserService userService;

    @Test
    @Transactional
    void findAllUser() {
        userRepository.findAll().forEach(user -> {
            user.getUserAddressList().forEach(data -> {
                System.out.println(data);
            });
        });
    }

    @Test
    void findAllUserAddress() {
        userAddressRepository.findAll().forEach(userAddress -> {
            System.out.println(userAddress);
            System.out.println(userAddress.getUser());
        });
    }

    @Test
    void findUserByID() {
        System.out.println(userRepository.findByUserId(19L));
    }

    @Test
    void setUserInfor() {
        UserDTO userDTO = new UserDTO();
        userDTO.setFullname("Duc Pham");
        userDTO.setPhoneNumber("9999999999");
        userDTO.setUserId(19L);
        userService.updateUserInfor(userDTO);
    }

    @Test
    void checkPass() {
//        System.out.println(userService.checkPassword("pad1092", 19L));
    }
}

package com.padshop;

import com.padshop.entities.Product;
import com.padshop.repositories.ProductRepository;
import com.padshop.repositories.PromotionRepository;
import com.padshop.services.ProductService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest
public class ProductTest {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ProductService productService;
    @Autowired
    PromotionRepository promotion;

    @Test
    @Transactional
    void findAllProduct() {
        productRepository.findAll().forEach(product -> {
            System.out.println(product);
            System.out.println(product.getProductImages());
            System.out.println(product.getProductQuantities());
            System.out.println("--------------------------");
        });
    }
    @Test
    @Transactional
    void testSlug(){
//        System.out.println(productService.toDTO(productRepository.getById(16L)));
        productRepository.findAll().forEach(product -> {
            String slug = productService.toDTO(product).getSlug();
            Long id = productRepository.findProductIdBySlug(slug);
            System.out.println("Slug: " + slug);
            System.out.println("Product: " + id);
            System.out.println("------------");
        });
    }

    @Test
    @Transactional
    void getProductDTo() {
//        System.out.println(productService.getProductDTOById(2L));
    }

    @Test
    @Transactional
    void getProductById() {
        Product product = productRepository.findProductByProductId(2L);
        if (product != null)
            product.getProductQuantities().forEach(data -> System.out.println(data));
    }

    @Test
    @Transactional
    @Rollback(value = false)
    void removePromotion() {
//        Promotion p = promotion.findByPromotionId(29L);
//        for(Product product: productRepository.findAllByProductPromotionPromotionId(29L))
//            product.setProductPromotion(null);
//        p.setPromotionProducts(null);
//        promotion.delete(p);
    }

    @Test
    @Transactional
    void getListProductNotDiscount() {
        for (Product data : productRepository.findAllByProductPromotionNull()) {
            System.out.println(data.getProductPromotion());
        }
    }

    @Test
    @Transactional
    @Rollback(value = false)
    void removePromotionO1() {
//        productService.removePromotion(12L);
    }

    @Test
    @Transactional
    void getProductIPByName() {
        List<Product> p = productRepository.getProductIPByName("", 33L);
        for (Product data : p) {
            System.out.println(data.getProductPromotion().getDiscount());
        }
    }

    @Test
    void testPaged() {
        productRepository.findAllByCodeLike("APL%").forEach(data -> {
            System.out.println(data);
        });
    }
}

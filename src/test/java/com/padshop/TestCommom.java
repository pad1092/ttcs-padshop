package com.padshop;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootTest
public class TestCommom {
    @Autowired
    Cloudinary cloudinary;

    @Test
    void testDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        System.out.println(new Date());
    }

    @Test
    void testSplit() {
        String url = "https://res.cloudinary.com/padshop/image/upload/v1656951314/xmecsqhdisv0n144v1bd.jpg";
        String imageName = url.split("/")[url.split("/").length - 1];
        String id = imageName.split("\\.")[0];
        try {
            cloudinary.uploader().destroy(id, ObjectUtils.emptyMap());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

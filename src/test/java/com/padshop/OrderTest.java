package com.padshop;

import com.padshop.repositories.OrderRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@SpringBootTest
public class OrderTest {
    @Autowired
    OrderRepository orderRepository;

    @Test
    @Transactional
    void findOrderByUserId() {
        LocalDate end = LocalDate.now();
        LocalDate start = end.minusDays(6);
        System.out.println(orderRepository.findRevenueByDay(start, end));
    }
    @Test
    @Transactional
    void testRevenue(){
        orderRepository.getTotalRevenue(5, 2023).forEach(t -> {

            System.out.println(t[0]);
        });
    }

}

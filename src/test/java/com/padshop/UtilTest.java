package com.padshop;

import com.padshop.services.UserService;
import com.padshop.util.MailSenderUtil;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.TimeZone;

@SpringBootTest
public class UtilTest {
    @Autowired
    private MailSenderUtil sendMail;
    @Autowired
    private UserService userService;

    @Test
    public void sendMail() {
        Instant currentTimestamp = Instant.now();
        ZoneId zoneId = ZoneId.systemDefault(); // Múi giờ tại máy tính
        ZoneId targetZoneId = ZoneId.of("GMT+7"); // Múi giờ +7

        ZonedDateTime currentDateTime = currentTimestamp.atZone(zoneId);
        ZonedDateTime createDateTime = currentDateTime.withZoneSameInstant(targetZoneId);
        ZonedDateTime expireDateTime = createDateTime.plusMinutes(15);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        String vnp_CreateDate = createDateTime.format(formatter);
        String vnp_ExpireDate = expireDateTime.format(formatter);

    }
}
